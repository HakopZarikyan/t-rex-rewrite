#ifndef PIDCONTROLLERS_HPP_
#define PIDCONTROLLERS_HPP_
#include "main.h"

extern PID LeftDrive;
extern PID RightDrive;

extern PID TurnLeftSide;
extern PID TurnRightSide;

extern PID ForbarPID;
extern PID Lift;

extern PID Straight;
#endif
