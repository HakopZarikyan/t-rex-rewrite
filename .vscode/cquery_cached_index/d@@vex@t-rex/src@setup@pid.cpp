#include "PID.hpp"
#include <iostream>
PID::PID(float kP, float kI, float kD)
{
  PID::kP = kP;
  PID::kI = kI;
  PID::kD = kD;
}
PID::~PID()
{}
void PID::SetTarget(float x)
{
  Target = x;
}
void PID::ChangeScalers(float kP, float kI, float kD)
{
  PID::kP = kP;
  PID::kI = kI;
  PID::kD = kD;
}
int PID::Compute(int SensorValue)
{
  Error = Target - SensorValue;
  Integral = Integral + Error;
  Derivative = LastSens - SensorValue;
  LastSens = SensorValue;
  Filtered = Filtered * 0.4 + Derivative * 0.6;
  if(std::abs(Error) > 400 || std::abs(Error) < 5){
    Integral = 0;
  }
  float Output = Error * kP + Integral * kI + Filtered * kD;
  return Output;
}
