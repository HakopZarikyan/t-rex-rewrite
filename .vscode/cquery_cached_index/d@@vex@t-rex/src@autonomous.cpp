#include "main.h"
#include "Global/Tasks.hpp"
/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
 void SetPower(int x, int y)
 {
   FR.move(y);
   BR.move(y);
   FL.move(x);
   BL.move(x);
 }
 void DriveTask(void *)
 {
   while( true)
   {
     float RightSide = FR.get_position() * 0.6 + BR.get_position() * 0.4;
     float LeftSide = FL.get_position() * 0.6 + BL.get_position() * 0.4;
     SetPower(drive.LeftDrive.Compute(LeftSide), drive.RightDrive.Compute(RightSide));

     delay(20);
   }
 }
 pros::Task DriveT(DriveTask);

void autonomous() {
  DriveT.suspend();
  drive.GoTo(360);
  delay(20);
  drive.Wait();

}
