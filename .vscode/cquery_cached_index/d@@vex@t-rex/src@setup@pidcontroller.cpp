#include "Global/PIDControllers.hpp"

PID LeftDrive(0, 0, 0);
PID RightDrive(0, 0, 0);

PID TurnLeftSide(0, 0, 0);
PID TurnRightSide(0, 0, 0);

PID ForbarPID(0, 0, 0);
PID Lift(0, 0, 0);

PID Straight(0, 0, 0);
