#include "main.h"
#include "Drive.hpp"
#ifndef GLOBALS_HPP
#define GLOBALS_HPP

extern pros::Motor FL;
extern pros::Motor FR;
extern pros::Motor BL;
extern pros::Motor BR;
extern pros::Motor Intake;
extern pros::Motor Rlift;
extern pros::Motor Llift;
extern pros::Motor Deploy;
extern pros::Controller Master;
extern pros::ADIEncoder Left;
extern pros::ADIEncoder Right;
extern pros::ADIEncoder Back;
extern pros::ADIPotentiometer LiftPot;
extern pros::ADIPotentiometer FPot;

extern Drive drive;

#endif
