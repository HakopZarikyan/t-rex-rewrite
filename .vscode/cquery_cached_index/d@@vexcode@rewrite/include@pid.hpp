class PID{
public:
  PID(float, float, float);
  ~PID();
  void SetTarget(float x);
  void ChangeScalers(float, float, float);
  int Compute(int SensorValue);
  float Error;
  float Derivative;
  float Integral;
  float Filtered;
private:
  float LastSens;
  float kP;
  float Target;
  float kI;
  float kD;
};
