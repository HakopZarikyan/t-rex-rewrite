#include "Global/Globals.hpp"

pros::Motor FL(2);
pros::Motor FR(9, true);
pros::Motor BL(1);
pros::Motor BR(10, true);
pros::Motor Intake(7);
pros::Motor Deploy(4);
pros::Motor Llift(3);
pros::Motor Rlift(8, true);
pros::Controller Master(pros::E_CONTROLLER_MASTER);
pros::ADIEncoder Left(5, 6);
pros::ADIEncoder Right(7, 8);
pros::ADIEncoder Back(3, 4);
pros::ADIPotentiometer FPot(1);
pros::ADIPotentiometer LiftPot(2);

Drive drive(0,0,0);
