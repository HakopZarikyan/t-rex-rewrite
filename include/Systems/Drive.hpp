#pragma once
#include "PID.hpp"
class Drive
{
public:
  Drive(float, float, float);
  ~Drive();
  PID LeftDrive = PID(0, 0, 0);
  PID RightDrive = PID(0, 0, 0);
  void GoTo(float);
  void DriveTask(void*);
  void Wait();
  void SetTimeout(long x);
  long timeOut;
private:
  void Power(float x, float y);
};
