#pragma once
#include "PID.hpp"
class Lift{
public:
  Lift(float, float, float);
  ~Lift();
  PID LiftPID = PID(0,0, 0);
  void SetLiftPosition(float Pos);
  void Wait();
  void LiftTask(void*);
};
