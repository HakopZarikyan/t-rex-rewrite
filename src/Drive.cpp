
#include "Systems/Drive.hpp"
#include "Global/Globals.hpp"
#include "api.h"
using namespace pros;

Drive::Drive(float kP, float kI, float kD)
{
  LeftDrive =  PID(kP, kI, kD);
  RightDrive =  PID(kP, kI, kD);
}

void Drive::GoTo(float x)
{
  LeftDrive.SetTarget(x);
  RightDrive.SetTarget(x);
}
void Drive::Power(float x, float y)
{
  FR.move(y);
  BR.move(y);
  FL.move(x);
  BR.move(x);
}
void Drive::SetTimeout(long x)
{
  timeOut = x;
}
void Drive::Wait()
{
  int timer = 0;
  while(timer < timeOut && (std::abs(LeftDrive.Error) > 5 || std::abs(RightDrive.Error) > 5))
  {
    timer++;
    delay(1);
  }
  timer = 0;
}
Drive::~Drive()
{}
